async function run() {
  const { REPO_SLUG, REPO, OWNER, BRANCH, PULL_REQUEST } = process.env
  console.log({ REPO_SLUG, REPO, OWNER, BRANCH, PULL_REQUEST })
}

run()
